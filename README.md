# CS371g: Generic Programming Collatz Repo

* Name: Ewin Jiao Zuo

* EID: ejz273

* GitLab ID: eztso

* HackerRank ID: ewinzuo

* Git SHA: b27f5464e0a859ed010c8ae8edf6ff50e39dce2d

* GitLab Pipelines: https://gitlab.com/eztso/cs371g-collatz/-/pipelines

* Estimated completion time: 10 Hours

* Actual completion time: 7 Hours

* Comments: Docker image does not have GraphViz installed which is a dependency for
doxygen.
