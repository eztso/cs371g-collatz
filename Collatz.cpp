// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

int cache[1000001] = {0};
bool cached = false;

// ------------
// collatz_read
// ------------


//build cache before first query is made
//dp-like eager cache
void build_cache() {
    cache[1] = 1;
    for (long i = 2; i < 1000001; ++i) {
        long temp = i;
        int len = 0;
        while (temp != 1 && temp >= i) {
            if (temp % 2 == 0) {
                temp = temp / 2;
                ++len;
            }
            else {
                temp = temp + (temp / 2) + 1;
                len += 2;
            }
        }
        cache[i] = cache[temp] + len;
    }
    cached = true;
}

pair<int, int> collatz_read (istream_iterator<int>& begin_iterator) {
    int i = *begin_iterator;
    ++begin_iterator;
    int j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}

// ------------
// collatz_eval
// ------------

tuple<int, int, int> collatz_eval (const pair<int, int>& p) {
    //build cache before first read
    if(!cached) {
        build_cache();
    }
    int i;
    int k;
    tie(i, k) = p;
    int ret_i = i;
    int ret_k = k;

    if(i>k) {
        std::swap(i, k);
    }
    if(i <= k/2) {
        i = k/2 + 1;
    }



    int res = 0;
    //values not necessarily ascending
    //find max value in range
    for(int idx = i; idx <= k; ++idx) {
        if (cache[idx] > res) {
            res = cache[idx];
        }
    }
    return make_tuple(ret_i, ret_k, res);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    istream_iterator<int> begin_iterator(sin);
    istream_iterator<int> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_print(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}
