// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 10));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<int, int, int> t = collatz_eval(make_pair(100, 200));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<int, int, int> t = collatz_eval(make_pair(201, 210));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<int, int, int> t = collatz_eval(make_pair(900, 1000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}

// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}



//student tests
// build_cache
TEST(CollatzFixture, build_cache1) {
    build_cache();
    ASSERT_EQ(cache[1], 1);
}

TEST(CollatzFixture, build_cache2) {
    build_cache();
    for(int i = 0; i < 1000001; i++) {
        ASSERT_TRUE(cache[1]!= 0);
    }
}

//read
TEST(CollatzFixture, read2) {
    istringstream         iss("1 1000000\n");
    istream_iterator<int> begin_iterator(iss);
    pair<int, int> p = collatz_read(begin_iterator);
    int i;
    int j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 1000000);
}

//print
TEST(CollatzFixture, print2) {
    ostringstream oss;
    collatz_print(oss, make_tuple(1, 1000000, 525));
    ASSERT_EQ(oss.str(), "1 1000000 525\n");
}

//eval
TEST(CollatzFixture, eval4) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 1000000));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1);
    ASSERT_EQ(j, 1000000);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval5) {
    tuple<int, int, int> t = collatz_eval(make_pair(1000000, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1000000);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 525);
}

TEST(CollatzFixture, eval6) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 1));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval7) {
    tuple<int, int, int> t = collatz_eval(make_pair(1, 837799));
    int i;
    int j;
    int v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 1);
    ASSERT_EQ(j, 837799);
    ASSERT_EQ(v, 525);
}

//solve
TEST(CollatzFixture, solve2) {
    istringstream iss("1 1000000\n1 1\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 1000000 525\n1 1 1\n", oss.str());
}

TEST(CollatzFixture, solve3) {
    istringstream iss("1 837799\n1 837798\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 837799 525\n1 837798 509\n", oss.str());
}
